#include "beamdash.h"
#include "bind.h"

#include <algorithm>
#include <iostream>
#include <iostream>
#include <imgui.h>
#include <implot.h>
#include "imgui_impl/imgui_impl_sdl.h"
#include "imgui_impl/imgui_impl_opengl3.h"
#include "fonts/fa-solid-900.h"
#include <imgui_internal.h>
#include <icon_fonts/IconsFontAwesome5.h>
#include <spdlog/spdlog.h>

#define USE_DOUBLE_BUFFERING 1

namespace beamdash {

void Beamdash::frame() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // Poll for SDL events.
    SDL_Event ev;
    while (SDL_PollEvent(&ev))
    {
        ImGui_ImplSDL2_ProcessEvent(&ev);
        switch (ev.type) {
            case SDL_QUIT:
                mQuit = true;
                break;
        }
    }

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(mWindow);
    ImGui::NewFrame();

    gui();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    SDL_GL_SwapWindow(mWindow);
}

void Beamdash::gui() {
    ImGui::ShowDemoWindow();
    ImPlot::ShowDemoWindow();
}

void Beamdash::destroy() {
    ImGui::GetIO().Fonts->Clear();
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImPlot::DestroyContext();
    ImGui::DestroyContext();
    SDL_DestroyWindow(mWindow);
    SDL_Quit();
}

Beamdash::Beamdash()
    : mFrameTimer { mIOContext }
{}

void Beamdash::run() {

    _initSDL();
    _initImGui();
    _guiLoop();
    mIOContext.run();
    destroy();
}


void Beamdash::_initSDL() {

    if (SDL_Init(SDL_INIT_VIDEO)) {
        SPDLOG_ERROR("Failed to init SDL video: {}", SDL_GetError());
    }

    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }
    if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }
    if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, USE_DOUBLE_BUFFERING)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }
    if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE)) { SPDLOG_ERROR("Failed SetAttribute: {}", SDL_GetError()); }

    mWindow = SDL_CreateWindow(
        "beamdash", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_UTILITY | SDL_WINDOW_ALLOW_HIGHDPI
    );
    if (!mWindow) {
        SPDLOG_ERROR("Failed to create SDL window: {}", SDL_GetError());
    }

    if (!(mGLContext = SDL_GL_CreateContext(mWindow))) {
        SPDLOG_ERROR("Failed to create gl context: {}", SDL_GetError());
    }
    glewExperimental = GL_TRUE;
    if (const GLenum err = glewInit()) {
        SPDLOG_ERROR("Failed to initialise glew: {}", glewGetErrorString(err));
    }

    SDL_SetWindowMinimumSize(mWindow, 640, 480);
    SDL_GL_SetSwapInterval(0);

    int width, height;
    SDL_GetWindowSize(mWindow, &width, &height);
    glViewport(0, 0, width, height);

    SPDLOG_INFO("Initialised SDL/GL");
}

void Beamdash::_initImGui() {

    ImGui::CreateContext();
    ImPlot::CreateContext();
    ImGui::StyleColorsLight();

    if (!ImGui_ImplSDL2_InitForOpenGL(mWindow, mGLContext)) {
        SPDLOG_ERROR("Failed to initialise ImGui for SDL2");
    }
    if (!ImGui_ImplOpenGL3_Init("#version 150")) {
        SPDLOG_ERROR("Failed to initialise ImGui for OpenGL");
    }

    _initFonts();
}

void Beamdash::_initFonts() {
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->AddFontDefault();
    static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_16_FA, 0 };
    ImFontConfig icons_config;
    icons_config.MergeMode = true;
    icons_config.PixelSnapH = true;
    icons_config.FontDataOwnedByAtlas = false;
    const float font_size = 13.0f;
    io.Fonts->AddFontFromMemoryTTF(fa_solid_900_ttf, fa_solid_900_ttf_len, font_size, &icons_config, icons_ranges);
}

void Beamdash::_guiLoopHandler(const std::error_code& error) {
    _guiLoop();
}

void Beamdash::_guiLoop() {
    auto frame_start_time = std::chrono::time_point_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now()
    );
    frame();

    // Schedule another frame.
    if (!mQuit) {
        auto next_frame_time = frame_start_time + std::chrono::microseconds(10000);
        mFrameTimer.expires_at(next_frame_time);
        mFrameTimer.async_wait(beamdash::bind_front(&Beamdash::_guiLoopHandler, this));
    }
    else {

    }
}

}