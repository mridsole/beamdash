#pragma once

#include <pybind11/pybind11.h>
#include <boost/asio.hpp>
#include <boost/container/flat_map.hpp>
#include <map>
#include <unordered_map>
#include <vector>
#include <memory>

namespace beamdash {

template <typename T> using uptr = std::unique_ptr<T>;
template <typename T> using sptr = std::shared_ptr<T>;
template <typename T> using vec = std::vector<T>;
template <typename K, typename V> using umap = std::unordered_map<K, V>;
template <typename K, typename V> using map = std::map<K, V>;
template <typename K, typename V> using flmap = boost::container::flat_map<K, V>;


namespace asio = boost::asio;
namespace py = pybind11;

}