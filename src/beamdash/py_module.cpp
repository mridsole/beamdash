#include "bindings.h"
#include <pybind11/pybind11.h>

PYBIND11_MODULE(beamdash_, m) {
    beamdash::init(m);
}