#pragma once

// Define this before including spdlog.h
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG

#include <spdlog/spdlog.h>

namespace beamdash {

bool logLevelFromString(const std::string& level_str, spdlog::level::level_enum& level);
void initLogging(spdlog::level::level_enum log_level = spdlog::level::info);

}

#define LOG_FORMATTER(T, format_string, value, ...) template<>\
struct fmt::formatter<T> {\
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {\
        return ctx.end();\
    }\
    template <typename FormatContext>\
    auto format(const T& value, FormatContext& ctx) -> decltype(ctx.out()) {\
        return format_to(ctx.out(), format_string, ##__VA_ARGS__);\
    }\
}
