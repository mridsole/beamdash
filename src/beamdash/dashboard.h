#pragma once

#include "prelude.h"

namespace beamdash {

/** Live class for a dashboard. Contains widgets and references to data connectors. */
class Dashboard {
public:
    Dashboard() = default;
    ~Dashboard() = default;

    void gui() {};

    py::object connector;
};

}