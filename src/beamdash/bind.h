#pragma once

#include <functional>

namespace beamdash {

template<typename F, typename... FRONT_ARGS>
auto bind_front(F&& f, FRONT_ARGS&&... front_args)
{
    // front_args are copied because multiple invocations of this closure are possible
    return [captured_f = std::forward<F>(f), front_args...](auto&&... back_args) {
                return std::invoke(captured_f, front_args...,
                                    std::forward<decltype(back_args)>(back_args)...);
            };
}

}