#pragma once

#include "prelude.h"

#include <unordered_map>

namespace beamdash {

/** The BeamDash context contains global state and resources, including:
 *  - The main ASIO IO context
 *  - All available data connectors
 */
struct Context {
    asio::io_context ioContext;
    // std::unordered_map<std::string, uptr<DataConnector>> dataConnectors;
};

}