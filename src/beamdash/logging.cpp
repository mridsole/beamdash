#include "logging.h"
#include <spdlog/spdlog.h>
#include <spdlog/async.h>

namespace beamdash {

bool logLevelFromString(const std::string& level_str, spdlog::level::level_enum& level) {
    if (level_str == "TRACE") {
        level = spdlog::level::trace;
        return true;
    }
    if (level_str == "DEBUG") {
        level = spdlog::level::debug;
        return true;
    }
    if (level_str == "INFO") {
        level = spdlog::level::info;
        return true;
    }
    if (level_str == "WARN") {
        level = spdlog::level::warn;
        return true;
    }
    if (level_str == "ERR") {
        level = spdlog::level::err;
        return true;
    }
    if (level_str == "CRITICAL") {
        level = spdlog::level::critical;
        return true;
    }
    if (level_str == "OFF") {
        level = spdlog::level::off;
        return true;
    }
    return false;
}

void initLogging(spdlog::level::level_enum log_level) {
    spdlog::init_thread_pool(8192, 1);
    spdlog::set_level(log_level);
    auto sink = std::make_shared<spdlog::sinks::ansicolor_stdout_sink_st>();
    auto logger = std::make_shared<spdlog::async_logger>("name", sink, spdlog::thread_pool(), spdlog::async_overflow_policy::overrun_oldest);
    logger->set_pattern("%Y-%m-%d %H:%M:%S.%f [%l] (%s:%#) %v", spdlog::pattern_time_type::utc);
    spdlog::register_logger(logger);
}

}