#pragma once

#include <pybind11/pybind11.h>

namespace beamdash {

void init(pybind11::module& m);

} // namespace beamdash