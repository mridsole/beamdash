#pragma once

#include "prelude.h"
#include "context.h"
#include "dashboard.h"

#include <system_error>
#include <memory>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <boost/asio.hpp>
#include <capnp/message.h>

namespace beamdash {

namespace asio = boost::asio;

/** Top-level class for all application data. */
class Beamdash {
public:

    Beamdash();

    void run();

    const std::vector<uptr<Dashboard>>& dashboards() const { return mDashboards; }

private:
    void frame();
    void gui();
    void destroy();

    void _initSDL();
    void _initImGui();
    void _initFonts();
    void _guiLoop();
    void _guiLoopHandler(const std::error_code& error);

    SDL_Window* mWindow;
    SDL_GLContext mGLContext;
    asio::io_context mIOContext;
    asio::system_timer mFrameTimer;
    bool mQuit = false;

    std::vector<uptr<Dashboard>> mDashboards;
};

}