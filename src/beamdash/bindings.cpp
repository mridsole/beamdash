#include "bindings.h"
#include "beamdash.h"

namespace beamdash {

void init(pybind11::module& m) {

    m.def("hello", []() {
        return "Hello, world!";
    });

    // Top-level class for the application.
    // py::class_<Beamdash>(m, "Beamdash")
    //     ;
    
    m.def("run", []() {
        Beamdash app;
        app.run();
    });
}

}