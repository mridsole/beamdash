#include "beamdash.h"
#include <spdlog/spdlog.h>
#include <boost/filesystem.hpp>
#include <cstdlib>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


namespace fs = boost::filesystem;
namespace py = pybind11;

int main () {

    py::scoped_interpreter guard{};

    // On macos, we don't detect that we're in a venv automatically. We need to
    // check for the VIRTUAL_ENV environment variable. If it is set, we can
    // append to sys.path based on that.
    if (const char* virtual_env_path_s = std::getenv("VIRTUAL_ENV")) {
        fs::path lib_path = fs::path(virtual_env_path_s) / "lib";
        fs::path site_packages_path;
        for (const auto& child : fs::directory_iterator(lib_path)) {
            if (child.path().filename().string().starts_with("python")) {
                site_packages_path = child.path() / "site-packages";
                SPDLOG_INFO("virtualenv detected, adding site-packages path({}) to sys.path", site_packages_path.string());
                break;
            }
        }
        if (site_packages_path.empty()) {
            SPDLOG_ERROR("virtualenv({}) detected, but failed to find site-packages directory", virtual_env_path_s);
            exit(1);
        }
        auto sys = py::module_::import("sys");
        sys.attr("path").attr("insert")(1, site_packages_path.c_str());
    }

#if 0
    py::module_::import("cgombe");
    py::module_::import("gombe.gombui");
#endif

    beamdash::Beamdash bd;
    bd.run();
}